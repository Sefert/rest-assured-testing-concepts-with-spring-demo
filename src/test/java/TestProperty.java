import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class TestProperty {

    @Getter
    @Value("${google.api.key}")
    private String key;
}

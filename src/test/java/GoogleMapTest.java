import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;

//@RunWith(SpringRunner.class)  // needed for jUnit
@ActiveProfiles({"dev", "integration"})
@ContextConfiguration(classes = {TestProperty.class})
@TestPropertySource(locations = {"classpath:/test.properties"}, properties = {"example=stackAbuse"})
public class GoogleMapTest extends AbstractTestNGSpringContextTests{

    @Value("${google.api.key}")
    private String key;

    @Autowired
    TestProperty testProperty;

    @Test
    public void googleMapRequestReturnsExpectedResponse(){

        RestAssured.baseURI="https://maps.googleapis.com";

        //request headers, parameters, cookies
        given()
                .param("location","-33.8670522,151.1957362")
                .param("key", key)
                .param("radius","1500")
                .param("type","restaurant")
        .when()
                .get("/maps/api/place/nearbysearch/json")
        .then()
                .assertThat().statusCode(200)
                .and()
                .body("results[0].name", equalTo("QT Sydney"))
                .and()
                .contentType(ContentType.JSON)
                .and()
                .header("Server", "scaffolding on HTTPServer2");

        //example
        assertEquals(key, testProperty.getKey());
    }
}

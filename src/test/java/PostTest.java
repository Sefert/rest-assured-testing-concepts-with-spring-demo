import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import model.MapData;
import model.PlaceId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@ActiveProfiles({"dev", "integration"})
@ContextConfiguration(classes = {TestProperty.class})
@TestPropertySource(locations = {"classpath:/test.properties"})
public class PostTest extends AbstractTestNGSpringContextTests{

    @Value("${uri.key}")
    private String uriKey;

    @Value("${add.json.path}")
    private String addJsonPath;

    @Value("${delete.json.path}")
    private String deleteJsonPath;

    @Value("${base.uri}")
    private String uri;

    Properties properties;

    //another way of loading properties
    @SneakyThrows
    @BeforeTest
    public void loadProperties(){
        properties = new Properties();
        properties.load(new FileInputStream("src/test/resources/test.properties"));
    }


    @Test
    public void addAndRemoveNewLocationInJsonStatusOk() throws IOException {

        RestAssured.baseURI = uri;

        File postData = new File("src/test/resources/data/PostData.json");

        Response response =
            given()
                    .queryParam("key", uriKey)
                    .contentType(ContentType.JSON)
                    .body(new ObjectMapper().readValue(postData, MapData.class))
            .when()
                    .post(addJsonPath)
            .then()
                    .assertThat()
                        .statusCode(200)
                        .contentType(ContentType.JSON)
                        .body("status", equalTo("OK"))
                        .extract().response();

        //TODO: Move to after test process
        //Clean-up process
        String placeId = new JsonPath(response.asString()).get("place_id");
        given()
                .queryParam("key", uriKey)
                .contentType(ContentType.JSON)
                .body("{\"place_id\": \""+placeId+"\"}")
        .when()
                .delete(deleteJsonPath)
        .then()
                .assertThat()
                    .statusCode(200)
                    .contentType(ContentType.JSON)
                    .body("status", equalTo("OK"));
    }

    @Test
    public void addAndRemoveNewLocationInXmlStatusOk() throws IOException {

        RestAssured.baseURI = properties.getProperty("base.uri");

        File postData = new File("src/test/resources/data/PostData.xml");

        Response response =
                given()
                        .queryParam("key", uriKey)
                        .contentType(ContentType.XML)
                        .body(new XmlMapper().readValue(postData, MapData.class))
                .when()
                        .post(properties.getProperty("add.xml.path"))
                .then()
                        .assertThat()
                        .statusCode(200)
                        .contentType(ContentType.XML)
                        .body("response.status", equalTo("OK"))
                        .extract().response();

        //TODO: Move to after test process
        //Clean-up process
        PlaceId placeId = new PlaceId(new XmlPath(response.asString()).get("response.place_id"));
        given()
                .queryParam("key", uriKey)
                .contentType(ContentType.XML)
                .body(new XmlMapper().writeValueAsString(placeId))
        .when()
                .delete(properties.getProperty("delete.xml.path"))
        .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.XML)
                .body("response.status", equalTo("OK"));
    }
}


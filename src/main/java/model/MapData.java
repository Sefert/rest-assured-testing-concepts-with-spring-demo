package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class MapData {

    private Location location;
    private int accuracy;
    private String name;
    private String phoneNumber;
    private String address;
    @JacksonXmlProperty(localName = "type")
    private List<String> types;
    private String website;
    private String language;

}
